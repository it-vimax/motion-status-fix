-- Функция конвертации времени в определенную таймзону
DROP FUNCTION IF EXISTS convert_datetime_to_timezone;
delimiter $$
create function convert_datetime_to_timezone(datetime datetime, timezone_value varchar(50)) returns varchar(50)
begin
return DATE_FORMAT(convert_tz(datetime, @@session.time_zone, timezone_value), '%Y-%m-%d %H:%i:%s');
end
$$
delimiter ;
# select convert_datetime_to_timezone('2010-01-01 00:00:01', '-3:00') as convert_datetime_to_timezone;

-- Функция получения занчения разницы в часовом поясе пользователя
DROP FUNCTION IF EXISTS get_user_timezone_value;
delimiter $$
create function get_user_timezone_value(user_id integer) returns varchar(50)
begin
    declare user_timezone_value varchar(250) default '0';
SELECT tzt.value
into user_timezone_value
FROM timeZoneStatuses t
         inner join timeZoneTypes as tzt on tzt.id = t.statusTypeId
WHERE t.userId = user_id
order by t.id desc
    limit 1;
if user_timezone_value = '0' then
SELECT tzt.value
into user_timezone_value
FROM driversRules as d
         inner join timeZoneTypes as tzt on tzt.id = d.timeZoneId
where userId = user_id;
end if;

    if CONVERT(user_timezone_value, signed INTEGER) > 0 then
        set user_timezone_value = concat('+', user_timezone_value);
end if;

return concat(user_timezone_value, ':00');
end
$$
delimiter ;
# select get_user_timezone_value(2278) as get_user_timezone_value;

-- получение времени завершения вождения пользователя
DROP FUNCTION IF EXISTS get_finish_driving_datetime;
delimiter $$
create function get_finish_driving_datetime(user_id integer, start_date datetime) returns datetime
begin
    declare is_has_finish_date INTEGER DEFAULT 0; # 2 - is has
    declare finished INTEGER DEFAULT 0;
    declare local_date datetime;
    declare finish_date datetime default '2010-01-01 00:00:01';

    declare status_cursor cursor for select s.datetime
                                     from status as s
                                     where s.userId = user_id
                                       and s.dateTime >= start_date
                                     order by s.dateTime asc
                                                                                                                                 limit 2;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

open status_cursor;
getStatus:
    LOOP
        FETCH status_cursor INTO local_date;
        IF finished = 1 THEN
            LEAVE getStatus;
END IF;

        set is_has_finish_date = is_has_finish_date + 1;
        if is_has_finish_date = 2 then
            set finish_date = local_date;
end if;
end loop getStatus;

CLOSE status_cursor;

if finish_date = '2010-01-01 00:00:01' then
        set finish_date = now();
end if;

return finish_date;
end
$$
delimiter ;
# select get_finish_driving_datetime(728, '2019-01-02 04:14:50') as get_finish_driving_datetime;

-- главная функция получения данных от определенного времени
DROP PROCEDURE IF EXISTS show_motion_status;
delimiter $$
create procedure show_motion_status(start_date datetime, finish_date datetime)
begin
    declare total_rows text default '';
    declare finished INTEGER DEFAULT 0;
    declare status_id integer;
    declare user_id integer;
    declare local_start_datetime datetime;
    declare local_finish_datetime datetime;
    declare user_start_driving_with_timezone datetime;
    declare user_finish_driving_with_timezone datetime;
    declare user_count_diff_hours integer default 0;
    declare count_motion_statuses integer default 0;
    -- получаем список всех записей драйвинга с начала определенного времени
    DECLARE status_cursor
        cursor for
select s.id, s.userId, s.dateTime
from status as s
         left join users as u on u.id = s.userId
where s.status = 1
  and s.dateTime >= start_date
  and s.dateTime <= finish_date
  and u.companyPosition = 7
order by s.dateTime asc
    limit 1;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

drop table if exists temp_driving_violation;
CREATE TEMPORARY TABLE temp_driving_violation
    (
        id                  int auto_increment primary key,
        user_id             int,
        driving_row_id      int,
        total_driving_hours int,
        total_devise_hours  int
    );

    set @count_motion_rows := 0;

open status_cursor;

-- запускаем цикл по каждой записи
getStatus:
    LOOP
        FETCH status_cursor INTO status_id, user_id, local_start_datetime;
        IF finished = 1 THEN
            LEAVE getStatus;
END IF;
        -- получаем время завершения драйвинга
        set user_start_driving_with_timezone =
                convert_datetime_to_timezone(local_start_datetime, get_user_timezone_value(user_id));
        set local_finish_datetime = get_finish_driving_datetime(user_id, local_start_datetime);
        set user_finish_driving_with_timezone =
                convert_datetime_to_timezone(local_finish_datetime, get_user_timezone_value(user_id));
        set user_count_diff_hours =
                TIMESTAMPDIFF(HOUR, user_start_driving_with_timezone, user_finish_driving_with_timezone);
        -- получить количество часов
        if user_count_diff_hours > 0 then
            -- получить количесво отчетов от прибора и сравнить с количеством часов
            set @is_motion_exist := 0;
            set @engine_statuses_table_name := 'enginestatuses';
            -- если таблица существует
            set @engine_statuses_table_name =
                    concat(@engine_statuses_table_name, concat('_', DATE_FORMAT(local_start_datetime, '%Y-%m')));
            -- если таблица существует
SELECT COUNT(*)
into @is_motion_exist
FROM information_schema.tables
WHERE table_name = @engine_statuses_table_name;
if @is_motion_exist > 0 then
                set @query_string := concat(
                        'select count(*) into @count_motion_rows from ',
                        '`', @engine_statuses_table_name, '` as es',
                        ' left join users as u on u.id = es.userId',
                        ' where es.userId = ', user_id,
                        ' and u.companyPosition = 7',
                        ' and es.dateTime between "', local_start_datetime, '" and "', local_finish_datetime, '"',
                        ' and es.statusTypeId = 2',
                        ' order by es.dateTime desc'
                    );
PREPARE stmt FROM @query_string;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

if user_count_diff_hours > @count_motion_rows then

                    insert into temp_driving_violation(user_id, driving_row_id, total_driving_hours, total_devise_hours)
                    values (user_id, status_id, local_start_datetime, local_finish_datetime);

end if;
end if;
end if;
end loop getStatus;

close status_cursor;

select * from temp_driving_violation;
end
$$
delimiter ;

call show_motion_status('2019-01-02 04:14:50', '2019-01-20 04:14:50');
