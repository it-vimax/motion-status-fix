<?php

require_once 'DbRepository.php';
$config = require_once 'config.php';

$date_start = $config['date_start']; // время начала поиска записей по умолчанию
/** @var DateTimeImmutable $date_start_obj */
$date_start_obj = false; // время начала поиска записей в обьекте
/** @var DateTimeImmutable $date_finish_obj */
$date_finish_obj = false; // время окончания последней итерации в обьекте

$time_start = time(); // всремя запуска скрипта
$totalRows = 0; // общее количество записей
$iterationResult = 0; // текущее количество пройденых записей


// принимаем время с консоли или берем с файла последнее время, если есть
if (isset($argv[1])) {
	$date_start = $argv[1];
} else {
	if (file_exists($config['files']['runtime'])) {
		$runtime = json_decode(file_get_contents($config['files']['runtime']), true);
		if (isset($runtime['date_start'])) {
			$date_start = $runtime['date_start'];
		}
	}
}

//$date_start = '2021-02-26';

try {
	echo '<pre>';
	echo "Старт. Время старта: " . date('Y-m-d H:i:s', $time_start) . PHP_EOL;
	
	$dbStorage = new DbRepository($config);
	// получить общее количество записей
	$totalRows = $dbStorage->getTotalRows($date_start);
	
	while ($iterationResult < $totalRows) {
		if ($date_finish_obj) {
			$date_start_obj = $date_finish_obj;
		} else {
			$date_start_obj = new DateTimeImmutable($date_start);
		}
		
		$allDrivings = $dbStorage->getDrivingFromDate($date_start_obj->format('Y-m-d H:i:s'));
		
		start($dbStorage, $allDrivings, $config, $date_start_obj,  $date_finish_obj);
		
		$iterationResult += count($allDrivings);
		
		if ($date_finish_obj) {
			$runtime['date_start'] = $date_finish_obj->format('Y-m-d H:i:s');
			file_put_contents($config['files']['runtime'], json_encode($runtime));
			
			echo "Завершен цикл. Обработано записей : " . count($allDrivings) . "/ Всего: $iterationResult" . PHP_EOL;
		}
	}
	
	$time_finish = time();
	$runtimeTimeMinute = round(($time_finish - $time_start) / 60);
	
	echo "Финиш. Время завершения " . date('Y-m-d H:i:s', $time_finish) . " . Время работы скрипта $runtimeTimeMinute минут. Всего обработано записей: $iterationResult/$totalRows" . PHP_EOL;
} catch (PDOException $e) {
	print "Error!: " . $e->getMessage() . "<br/>";
	die();
}

$lastFinishDate = '';
function start($dbStorage, $allDrivings, $config, &$date_start_obj, &$date_finish_obj)
{
	foreach ($allDrivings as $driving) {
		$tzName = $dbStorage->getDriverTimeZoneName($driving['userId']);
		$tzName = $tzName ? $tzName : 'UTC';
		
		$date = $dbStorage->getStartAndFinishDateTime($driving['userId'], $driving['dateTime'], $tzName);
		if ($date) {
			$totalHours = $dbStorage->getCountHoursInDrivingStatus($date['startDate'], $date['finishDate']);
			if ($totalHours > 0) {
				$allMotionRows = $dbStorage->getAllMotionRows($driving['userId'], $date['startDate'], $date['finishDate']);
				if ($totalHours > count($allMotionRows)) {
					$userId = $driving['userId'];
					$motionTotalRows = count($allMotionRows);
					$date_start_obj = $date['startDate'];
					$date_finish_obj =  $date['finishDate'];
					$trueDateFinish = $date_start_obj->modify("+ $motionTotalRows hour");
					
					$resultStr =
						'Количество часов до смены татуса = ' . $totalHours
						. '; Записей с прибора = ' . $motionTotalRows
						. '; Пользователь: ' . $userId
						. '; Интервал времени: с ' . $date_start_obj->format('Y-m-d H:i:s') . ' по ' . $date_finish_obj->format('Y-m-d H:i:s')
						. '; Driving должен был закончится: ' . $trueDateFinish->format('Y-m-d H:i:s')
						. PHP_EOL;
					
					echo $resultStr;
					
					file_put_contents($config['files']['result'], $resultStr, FILE_APPEND);
				}
			}
		}
	}
}
