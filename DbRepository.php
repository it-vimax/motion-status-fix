<?php

class DbRepository
{
	/** @var PDO */
	private $pdo;
	private $config;
	
	/**
	 * Main constructor.
	 * @param $connect
	 */
	public function __construct($config)
	{
		$this->config = $config;
		$this->pdo = new PDO('mysql:host=' . $config['db']['host'] . ';dbname=' . $config['db']['dbname'], $config['db']['username'], $config['db']['password']);
	}
	
	public function getTotalRows($dateStart)
	{
		$sql = 'select count(s.id) FROM status as s
					WHERE s.status = ' . $this->config['status']['driving']
					. ' and s.dateTime >= "' . $dateStart . '"';
		
		$result = $this->pdo->query($sql)->fetch(PDO::FETCH_ASSOC);
		return reset($result);
	}
	
	public function getDriverTimeZoneName($userId)
	{
		$result = false;
		$sql = 'SELECT tzt.short FROM timeZoneStatuses t inner join timeZoneTypes as tzt on tzt.id = t.statusTypeId WHERE t.userId = '.$userId.' order by t.id desc limit 1';
		
		$resultFromTimeZoneStatuses = $this->pdo->query($sql)->fetch(PDO::FETCH_ASSOC);
		if (is_array($resultFromTimeZoneStatuses)) {
			$result = reset($resultFromTimeZoneStatuses);
		}
		
		if (!$result) {
			$sql = 'SELECT tzt.short FROM driversRules as d inner join timeZoneTypes as tzt on tzt.id = d.timeZoneId where userId = ' . $userId;
			$resultFromDriversRules = $this->pdo->query($sql)->fetch(PDO::FETCH_ASSOC);
			if (is_array($resultFromDriversRules)) {
				$result = reset($resultFromDriversRules);
			}
		}
		
		return $result;
	}
	
	public function getDrivingFromDate($startDateString)
	{
		$sql = '
			select * from ' . $this->config['tables']['status'] . ' as s
			where s.status = ' . $this->config['status']['driving'] . '
			and s.dateTime >= "' . $startDateString . '"
			order by s.dateTime asc
			limit ' .  $this->config['query_limit'] . '
		';
		
		return $this->pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function getStartAndFinishDateTime($userId, string $startDateTime, $tzName)
	{
		$sql = '
			select * from ' . $this->config['tables']['status'] . ' as s
			left join users as u on u.id = s.userId
			where s.userId = ' . $userId . '
			and u.companyPosition = ' . $this->config['status']['companyPosition'] . '
			and s.dateTime >= "' . $startDateTime . '"
			order by s.dateTime asc
			limit 2
		';
		
		$oneStatusInterval = $this->pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
		
		if (!empty($oneStatusInterval)) {
			$startDate = new DateTimeImmutable($oneStatusInterval[0]['dateTime'], new DateTimeZone($tzName));
			// получаем, дату следующего статуса, или текущую дату, если статус больше не менялся
			if (isset($oneStatusInterval[1])) {
				$finishDate = new DateTimeImmutable($oneStatusInterval[1]['dateTime'], new DateTimeZone($tzName));
			} else {
				$finishDate = (new DateTimeImmutable('now', new DateTimeZone($tzName)));
			}
			$finishDate->add(new DateInterval('PT' . $this->config['interval_diff_sec'] . 'S'));
			
			return compact('startDate', 'finishDate');
		}
		return false;
	}
	
	public function getCountHoursInDrivingStatus(DateTimeImmutable $firstDate, DateTimeImmutable $finishDate)
	{
		$totalHours = 0;
		$interval = $finishDate->diff( $firstDate);
		$totalHours += !empty($interval->y) ? $interval->y * 8766 : 0;
		$totalHours += !empty($interval->m) ? $interval->m * 730 : 0;
		$totalHours += !empty($interval->d) ? $interval->d * 24 : 0;
		$totalHours += $interval->h;
		
		return $totalHours;
	}
	
	public function isEngineStatusTableExist($suffix)
	{
		$result = $this->pdo->query('SHOW TABLES LIKE "' . $this->config['tables']['engine_status'] . $suffix . '"')->fetch(PDO::FETCH_ASSOC);
		return $result !== false;
	}
	
	public function getAllMotionRows($userId, DateTimeImmutable $startDate, DateTimeImmutable $finishDate)
	{
		$suffix = '_' . $startDate->format('Y-m');
		if ($this->isEngineStatusTableExist($suffix)) {
			$sql = 'select * from `' . $this->config['tables']['engine_status'] . $suffix . '` as es
						left join users as u on u.id = es.userId
						where es.userId = ' . $userId . '
						and u.companyPosition = ' . $this->config['status']['companyPosition'] . '
						and es.dateTime between "' . $startDate->format('Y-m-d H:i:s') . '" and "' . $finishDate->format('Y-m-d H:i:s') . '"
						and es.statusTypeId = ' . $this->config['status']['motion'] . '
						order by es.dateTime desc';
			return $this->pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
		}
		return [];
	}
}
