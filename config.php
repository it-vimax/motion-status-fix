<?php

@$local = include 'config.local.php';

if (!is_array($local)) {
	$local = [];
}

return array_merge([
	'db' => [
		'dbname' => 'admin_ezlogz_api',
		'username' => 'root',
		'password' => '',
		'host' => 'localhost',
	],
	'files' => [
		'runtime' => 'runtime.json',
		'result' => 'runtime_' . time() . '.log',
	],
	'date_start' => '2017-01-01',
	'tables' => [
		'status' => 'status',
		'engine_status' => 'enginestatuses',
	],
	'interval_diff_sec' => 5,
	'status' => [
		'driving' => 1,
		'motion' => 2,
		'companyPosition' => 7
	],
	'query_limit' => 10000,
], $local);